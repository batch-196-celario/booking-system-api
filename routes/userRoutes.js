const express = require("express");
const router = express.Router();
const userControllers = require("../controllers/userControllers")
const auth = require("../auth");



const {verify} = auth;



//console.log(userControllers)

router.post("/",userControllers.registerUser);

router.post('/details',verify,userControllers.getUserDetails);

router.post('/login',userControllers.loginUser)

router.post('/checkEmail',userControllers.checkUserEmail)

router.post('/enroll',verify,userControllers.enroll);

module.exports = router;